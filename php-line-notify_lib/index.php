<!DOCTYPE html>
<html lang="en">
<head>
  <title>PHP Line Notify ทดสอบส่งไลน์ผ่านเว็บด้วย PHP</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 1500px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;} 
    }
  </style>
</head>
<body>
<div class="container-fluid">
  <div class="row content">
  <form class="form-horizontal" method="post" action="line.class.php" enctype="multipart/form-data">
  <fieldset>
    <legend>PHP Line Notify ทดสอบส่งไลน์ผ่านเว็บด้วย PHP</legend>
    <div class="form-group">
      <label for="textArea" class="col-lg-2 control-label">ข้อความ</label>
      <div class="col-lg-10">
        <textarea class="form-control" rows="3" id="message"  name="message"></textarea>
        <span class="help-block">A longer block of help text that breaks onto a new line and may extend beyond one line.</span>
      </div>
    </div>
    <div class="form-group">
      <label for="textArea" class="col-lg-2 control-label">ข้อความ</label>
      <div class="col-lg-10">
        <input type="file" name="file" id="file">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-10 col-lg-offset-2">
        <button type="reset" class="btn btn-default">Cancel</button>
        <button type="submit" class="btn btn-primary" name="submit">Submit</button>
      </div>
    </div>
  </fieldset>
  <?php 
  if(isset($_GET['result']))
  {
    echo $_GET['result'];
  }
  ?>
</form>
</div>
</div>
</body>
</html>