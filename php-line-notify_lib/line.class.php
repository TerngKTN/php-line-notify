<?php
if($_POST)
{
    $class = new Line();
    $image_path =null;
    if($_FILES['file']['size'] != 0)
    {
        $image_path=$class->upload($_FILES["file"]);
    }
    $class->send_notify($_POST["message"],$image_path);
    header('Location: index.php');
}
class Line
{
  const LINE_TOKEN = 'TU76ipkKmAW56cKIVg5lfC6gDJlsS91lmt7iJPRuOGZ';//line test
  //const LINE_TOKEN = 'kZpAODxnMMW2apRlyGxe6JFJhHKDTwos0gDHgPkTUUL';//got it
  public function __construct()
  {
        date_default_timezone_set("Asia/Bangkok");
        require_once './vendor/autoload.php';
  }
  public function send_notify($message,$image_path)
  {
        $line = new KS\Line\LineNotify(Line::LINE_TOKEN);
        $image_path = './uploads/'.$image_path; //Line notify allow only jpeg and png file
        $line->send($message,$image_path);
  } 
  public function upload($file)
  {
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($file["name"]);
        if (copy($file["tmp_name"], $target_file)) 
        {
            return $file["name"];
        } 
        else 
        {
            return null;
        }
  }
}
?>